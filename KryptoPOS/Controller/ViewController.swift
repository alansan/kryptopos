//
//  ViewController.swift
//  KryptoPOS
//
//  Created by Alan Santoso on 16/01/20.
//  Copyright © 2020 Alan Santoso. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var tableView = UITableView()
    var pickerView = UIPickerView()
    
    var hasil: Hasil?
    let postRequest = APIRequest()

    
    struct Cells{
        static let textCell = "textCell"
        static let textAreaCell = "textAreaCell"
        static let datePickerCell = "datePickerCell"
        static let comboboxCell = "comboboxCell"
        static let checkboxCell = "checkboxCell"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTable()

//        addToList
//        updateList
//        deleteFromList()
        loadPayload()

    }
    
    func configureTable(){
        view.addSubview(tableView)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(TextTableViewCell.self, forCellReuseIdentifier: Cells.textCell)
        tableView.register(TextAreaTableViewCell.self, forCellReuseIdentifier: Cells.textAreaCell)
        tableView.register(DatePickerTableViewCell.self, forCellReuseIdentifier: Cells.datePickerCell)
        tableView.register(PickerViewTableViewCell.self, forCellReuseIdentifier: Cells.comboboxCell)
        tableView.register(CheckBoxTableViewCell.self, forCellReuseIdentifier: Cells.checkboxCell)

        tableView.rowHeight = 100
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        
    }
    
    func loadPayload(){
        let semaphore = DispatchSemaphore(value: 0)
        let dispatchQueue = DispatchQueue.global(qos: .background)
        
        dispatchQueue.async {
            
            self.postRequest.checkoutPayload { [weak self] (res) in
                switch res{
                case .success(let hasil):
                    self?.hasil = hasil
                    semaphore.signal()
                case .failure(let error):
                    print(error)
                }
            }
            semaphore.wait()
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }
    }
    
}


extension ViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hasil?.payload?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let payload = hasil?.payload?[indexPath.row]{
//            print(payload.input)
            
            switch payload.input {
            case "text":
                let cell = (tableView.dequeueReusableCell(withIdentifier: Cells.textCell, for: indexPath) as? TextTableViewCell)!
                cell.set(payload: payload)
                return cell
                
            case "textarea":
                let cell = (tableView.dequeueReusableCell(withIdentifier: Cells.textAreaCell, for: indexPath) as? TextAreaTableViewCell)!
                return cell
                
            case "datepicker":
                let cell = (tableView.dequeueReusableCell(withIdentifier: Cells.datePickerCell, for: indexPath) as? DatePickerTableViewCell)!
                return cell
                
            case "checkbox":
                let cell = (tableView.dequeueReusableCell(withIdentifier: Cells.checkboxCell, for: indexPath) as? CheckBoxTableViewCell)!
                return cell
                
            case "combobox":
                let cell = tableView.dequeueReusableCell(withIdentifier: Cells.comboboxCell, for: indexPath) as! PickerViewTableViewCell
                cell.setData(payload: payload)
                return cell
                
            default:
                let cell = (tableView.dequeueReusableCell(withIdentifier: Cells.textCell, for: indexPath) as? TextTableViewCell)!
                return cell
                
            }
            
        }
        return UITableViewCell()
    }
    
    
    
    
}
