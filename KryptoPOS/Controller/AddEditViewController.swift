//
//  AddEditViewController.swift
//  KryptoPOS
//
//  Created by Alan Santoso on 21/01/20.
//  Copyright © 2020 Alan Santoso. All rights reserved.
//

import UIKit

class AddEditViewController: UIViewController {
    
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var descriptionTF: UITextField!
    @IBOutlet weak var colorTF: UITextField!
    @IBOutlet weak var statusTF: UITextField!
    
    @IBOutlet weak var saveBtn: UIButton!
    
    
    let postRequest = APIRequest()
    
    var crud : CRUD?
    
    var hasil : Payload2?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        print(hasil)
        setupData()
    }
    
    
    
    func setupData(){
        nameTF.text = hasil?.crudName
        descriptionTF.text = hasil?.crudDescription
        statusTF.text = hasil?.crudStatus
        colorTF.text = hasil?.crudColor
    }
    
    
    @IBAction func saveTapped(_ sender: Any) {
        if hasil == nil {
            //save
            addToList()
        }else{
            //edit
            updateList()
        }
    }
    
    func addToList(){
        
        let newCrud = checkData()
        
        postRequest.addToList(crud: newCrud) { (res) in
            switch res {
            case .success(let hasil):
                DispatchQueue.main.async {
                    self.resetData()
                    self.addAlert()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func checkData() -> CRUD{
        let name = nameTF.text ?? "null"
        let description = descriptionTF.text ?? "null"
        let color = colorTF.text ?? "null"
        let status = statusTF.text ?? "null"
        
        let newCrud = CRUD(crud_name: name, crud_description: description, crud_color: color, crud_status: status)
        
        return newCrud
    }
    
    func resetData(){
        nameTF.text = ""
        descriptionTF.text = ""
        colorTF.text = ""
        statusTF.text = ""
    }
    
    func updateList(){
        let id = hasil?.crudId ?? ""
        let name = nameTF.text ?? ""
        let description = descriptionTF.text ?? ""
        let color = colorTF.text ?? ""
        let status = statusTF.text ?? ""
        
        
        let updatedCrud = CRUD(crud_id: id,crud_name: name, crud_description: description, crud_color: color, crud_status: status)
        
        postRequest.updateList(crud: updatedCrud) { (res) in
            switch res {
            case .success(let hasil):
                print(hasil)
                self.updateAlert()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func updateAlert(){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Alert", message: "Update Data Success", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert) in
                print("Ok Update")
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func addAlert(){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Alert", message: "Add Data Success", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert) in
                print("Ok Add")
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
}
