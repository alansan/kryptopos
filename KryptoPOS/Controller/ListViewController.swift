//
//  ListViewController.swift
//  KryptoPOS
//
//  Created by Alan Santoso on 21/01/20.
//  Copyright © 2020 Alan Santoso. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let postRequest = APIRequest()
    
    var hasil: HasilModel? {
        didSet{
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    var hasilForEdit :Payload2?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        readList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        readList()
    }
    
    func readList(){
        // MARK: - Read List
        postRequest.readList { (res) in
            switch res {
            case .success(let hasil):
                self.hasil = hasil
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @IBAction func addTapped(_ sender: Any) {
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "EditStoryboard") {
            let addEditViewController = segue.destination as! AddEditViewController
            addEditViewController.hasil = hasilForEdit
            
        }
        if segue.identifier == "AddStoryboard"{
            let addEditViewController = segue.destination as! AddEditViewController
            addEditViewController.hasil = nil
        }
        
    }
}


extension ListViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let total = hasil?.payload?.count else{
            return 0
        }
        
        return total
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as! ListTableViewCell
        
        cell.nameLabel.text = "Name: " + (hasil?.payload![indexPath.item].crudName ?? "null")
        cell.descriptionLabel.text = "Description: " + (hasil?.payload?[indexPath.item].crudDescription ?? "null")
        cell.colorLabel.text = "Color: " + (hasil?.payload![indexPath.item].crudColor ?? "null")
        cell.statusLabel.text = "Status: " + (hasil?.payload![indexPath.item].crudStatus ?? "null")
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let payload = hasil?.payload?[indexPath.row]{
            hasilForEdit = payload
        }
        
        self.performSegue(withIdentifier: "EditStoryboard",sender: indexPath.row)
        
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            if let id = hasil?.payload?[indexPath.row].crudId{
                deleteFromList(id: id)
                
            }
        }
    }
    
    func deleteFromList(id:String){
        postRequest.deleteFromList(id: id) { (res) in
            switch res {
            case .success(let _):
                self.deleteAlert()
                self.readList()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func deleteAlert(){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Alert", message: "Delete Data Success", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert) in
                print("Ok Delete")
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
}
