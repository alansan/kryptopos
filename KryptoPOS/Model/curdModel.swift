//
//  curdModel.swift
//  KryptoPOS
//
//  Created by Alan Santoso on 16/01/20.
//  Copyright © 2020 Alan Santoso. All rights reserved.
//

import Foundation
struct CRUD : Codable {
    
    let crud_id : String
    let crud_name : String
    let crud_description : String
    let crud_color : String
    let crud_status : String
    let crud_timestamp : String = "\(Date())"
    
    init(crud_id: String = UUID().uuidString,crud_name : String, crud_description: String, crud_color:String, crud_status: String){
        self.crud_id = crud_id
        self.crud_name = crud_name
        self.crud_color = crud_color
        self.crud_status = crud_status
        self.crud_description = crud_description
    }
}


// MARK: - Response
struct Hasil : Codable {
    let errorCode: Int
    let errorMessage: String
    let payload: [Payload]?
}

// MARK: - Payload
struct Payload : Codable {
    let field, type, input: String
    let maxLength : String?
    let accepted: String?
}

// MARK: - HasilModel
struct HasilModel: Codable {
    let errorCode: Int?
    let errorMessage: String?
    let payload: [Payload2]?
}

// MARK: - Payload
struct Payload2 :Codable {
    let crudId, crudName: String?
    let crudDescription: String?
    let crudColor, crudStatus, crudTimestamp: String?
}
