//
//  ComboBoxTableViewCell.swift
//  KryptoPOS
//
//  Created by Alan Santoso on 17/01/20.
//  Copyright © 2020 Alan Santoso. All rights reserved.
//

import UIKit

class ComboBoxTableViewCell: UITableViewCell {

    var segmentedControl = UISegmentedControl(items: ["abc", "def"])
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configuresegmentedControl()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init recorder need to be implemented")
    }

    
    func configuresegmentedControl(){
        addSubview(segmentedControl)

        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        segmentedControl.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 4).isActive = true
        segmentedControl.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        segmentedControl.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true

    }
    
    func set(payload : Payload){
        let fullColorName = payload.accepted
        let colorArray = fullColorName?.split{$0 == "|"}.map(String.init)
        
        if let colors = colorArray{
            segmentedControl.replaceSegments(segments: colors)
            for i in 0..<colors.count{
                segmentedControl.setTitle(colors[i], forSegmentAt: i)
            }
            
        }
    }

}


extension UISegmentedControl {
    func replaceSegments(segments: Array<String>) {
        self.removeAllSegments()
        for segment in segments {
            self.insertSegment(withTitle: segment, at: self.numberOfSegments, animated: false)
        }
    }
}
