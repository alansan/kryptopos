//
//  PickerViewTableViewCell.swift
//  KryptoPOS
//
//  Created by Alan Santoso on 18/01/20.
//  Copyright © 2020 Alan Santoso. All rights reserved.
//

import UIKit

class PickerViewTableViewCell: UITableViewCell {
    
    var pickerView = UIPickerView(frame: CGRect(x: 20, y: 20, width: 100, height: 40))
    var pickerData = [String]()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configuresegmentedControl()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init recorder need to be implemented")
    }
    
    
    func configuresegmentedControl(){
        addSubview(pickerView)
        
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        pickerView.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        pickerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 4).isActive = true
        pickerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        pickerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        
    }
    
    func setData(payload : Payload){
        let fullColorName = payload.accepted
        let colorArray = fullColorName?.split{$0 == "|"}.map(String.init)
        
        if let colors = colorArray {
            pickerData = colors
        }
        
    }
    
    
}

extension PickerViewTableViewCell : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    
}
