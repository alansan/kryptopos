//
//  APIRequest.swift
//  KryptoPOS
//
//  Created by Alan Santoso on 17/01/20.
//  Copyright © 2020 Alan Santoso. All rights reserved.
//

import Foundation


enum APIError : Error {
    case responseProblem
    case decodingProblem
    case otherProblem
}

struct APIRequest{
    let resourceURL: URL
    
    init(){
        let resourceString = "https://us-central1-shopick-io-cvm.cloudfunctions.net/crud"
        guard let resourceURL = URL(string: resourceString) else {fatalError()}
        
        self.resourceURL = resourceURL
    }
    
    
    func checkoutPayload(completion: @escaping(Result<Hasil,APIError>) -> ()){
        let parameters = ["a": "metadata",
        "p": "{}"] as [String : Any]
        var urlRequest = URLRequest(url: resourceURL)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.otherProblem))
        }
        
        
        let task = URLSession.shared.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            
            do {
                let json = try JSONDecoder().decode(Hasil.self, from: jsonData)
                completion(.success(json))
            } catch let error {
                completion(.failure(.decodingProblem))
            }
        })
        task.resume()
        
    }
    
    func readList(completion: @escaping(Result<HasilModel,APIError>) -> ()){
        let parameters = ["a": "list",
                          "p": "{}"] as [String : Any]
        var urlRequest = URLRequest(url: resourceURL)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.otherProblem))
        }
        
        let task = URLSession.shared.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let json = try decoder.decode(HasilModel.self, from: jsonData)
                completion(.success(json))
            } catch{
                completion(.failure(.decodingProblem))
            }
        })
        task.resume()
        
        
    }
    
    func deleteFromList(id :String ,completion: @escaping(Result<[String:Any],APIError>) -> ()){
        
        let parameters = ["a" : "delete",
                          "p":
                            [ "crud_id" : "\(id)"]
                        ] as [String : Any]
        
        var urlRequest = URLRequest(url: resourceURL)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.otherProblem))
        }
        
        let task = URLSession.shared.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String: Any]
                completion(.success(json!))
            } catch{
                completion(.failure(.decodingProblem))
            }
        })
        task.resume()
        
        
    }

    func addToList(crud :CRUD ,completion: @escaping(Result<[String:Any],APIError>) -> ()){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateNow = formatter.string(from: Date())
        
        let parameters = ["a": "insert",
                          "p": [
                            "crud_id": "\(crud.crud_id)",
                            "crud_name": "\(crud.crud_name)",
                            "crud_description": "\(crud.crud_description)",
                            "crud_color" : "\(crud.crud_color)",
                            "crud_status" : "\(crud.crud_status)",
                            "crud_timestamp" : "\(dateNow)",
                            
            ]
            ] as [String : Any]
        
        var urlRequest = URLRequest(url: resourceURL)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.otherProblem))
        }
        
        let task = URLSession.shared.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String: Any]
                completion(.success(json!))
            } catch{
                completion(.failure(.decodingProblem))
            }
        })
        task.resume()
        
        
    }

    func updateList(crud :CRUD ,completion: @escaping(Result<[String:Any],APIError>) -> ()){
        let parameters = ["a": "update",
                          "p": [
                            "crud_id": "\(crud.crud_id)",
                            "crud_name": "\(crud.crud_name)",
                            "crud_description": "\(crud.crud_description)",
                            "crud_color" : "\(crud.crud_color)",
                            "crud_status" : "\(crud.crud_status)",
                            
            ]
            ] as [String : Any]
        print(parameters)
        
        var urlRequest = URLRequest(url: resourceURL)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.otherProblem))
        }
        
        let task = URLSession.shared.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String: Any]
                completion(.success(json!))
            } catch{
                completion(.failure(.decodingProblem))
            }
        })
        task.resume()
        
        
    }

}
